from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL

# initializations
app = Flask(__name__)

# Mysql Connection
app.config['MYSQL_HOST'] = 'localhost' 
app.config['MYSQL_USER'] = 'root1'
app.config['MYSQL_PASSWORD'] = 'root'
app.config['MYSQL_DB'] = 'taller'
mysql = MySQL(app)

# settings
app.secret_key = "mysecretkey"

# routes
@app.route('/') #@app.route son los decoradores
def Index():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM sistema')
    data = cur.fetchall()
    cur.close()
    return render_template('index.html', contacts = data)

@app.route('/add_contact', methods=['POST'])
def add_contact():
    if request.method == 'POST':
        nombre = request.form['nombre']
        apellido = request.form['apellido']
        estado = request.form['estado']
        localizacion = request.form['localizacion']
        tipo_cliente = request.form['tipo_cliente']
        fecha = request.form['fecha']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO sistema (nombre, apellido, estado, localizacion, tipo_cliente, fecha) VALUES (%s,%s,%s,%s,%s,%s)", (nombre, apellido, estado, localizacion, tipo_cliente, fecha))
        mysql.connection.commit()
        flash('Registro Guardado Exitosamente.')
        return redirect(url_for('Index'))

@app.route('/edit/<id_cliente>', methods = ['POST', 'GET'])
def get_contact(id_cliente):
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM sistema WHERE id_cliente = %s', (id_cliente))
    data = cur.fetchall()
    cur.close()
    print(data[0])
    return render_template('edit-contact.html', contact = data[0])

@app.route('/update/<id>', methods=['POST'])
def update_contact(id_cliente):
    if request.method == 'POST':
        id_cliente = request.form['id_cliente']
        nombre = request.form['nombre']
        apellido = request.form['apellido']
        estado = request.form['estado']
        localizacion = request.form['localizacion']
        tipo_cliente = request.form['tipo_cliente']
        fecha = request.form['fecha']
        cur = mysql.connection.cursor()
        cur.execute("""
            UPDATE contacts
            SET nombre = %s,
                apellido = %s,
                estado = %s,
                localizacion = %s,
                tipo_cliente = %s,
                fecha = %s
            WHERE id_cliente = %s
        """, (nombre, apellido, estado, localizacion, tipo_cliente, fecha, id_cliente))
        flash('Registro Actualizado Exitosamente.')
        mysql.connection.commit()
        return redirect(url_for('Index'))

@app.route('/delete/<string:id>', methods = ['POST','GET'])
def delete_contact(id):
    cur = mysql.connection.cursor()
    cur.execute('DELETE FROM sistema WHERE id_cliente = {0}'.format(id))
    mysql.connection.commit()
    flash('Archivo Borrado Exitosamente.')
    return redirect(url_for('Index'))

# starting the app
if __name__ == "__main__":
    app.run(port=3000, debug=True)
